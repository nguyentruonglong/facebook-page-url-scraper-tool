# Directory paths for input and output data
INPUT_FILE_DIR = 'data/input/'              # Directory for input data
OUTPUT_FILE_DIR = 'data/output/'            # Directory for output data
OUTPUT_LOG_DIR = 'data/output/logs/'        # Directory for log files
COOKIE_FILE_DIR = 'data/input/cookies/cookies.inpt'             # Directory for cookie input file
PROXY_FILE_DIR = 'data/input/proxies/500_proxies.inpt'          # Directory for proxy input file
OUTPUT_SCREENSHOT_DIR = 'data/output/screenshots/'              # Directory for screenshots
OUTPUT_RESPONSE_DIR = 'data/output/responses/'                  # Directory for HTTP responses
INPUT_CATEGORY_DIR = 'data/input/categories/'                    # Directory for input categories
USER_AGENT_FILE_DIR = 'data/input/user_agents/use_agents.txt'   # Directory for user agent input file
MINUTELY_REPORT = 'data/output/analysis/minutely_report.txt'     # Directory for minutely reports
HOURLY_REPORT = 'data/output/analysis/hourly_report.txt'         # Directory for hourly reports
DAILY_REPORT = 'data/output/analysis/daily_report.txt'           # Directory for daily reports

# Timezone setting
TIMEZONE = 'Asia/Saigon'

# Timeout settings
TIMEOUT = 60                             # Default timeout for various operations

# Web scraping options
USE_COOKIE = True                        # Flag to indicate whether to use cookies
USE_USER_AGENT = True                    # Flag to indicate whether to use user agents
USE_PROXY = True                         # Flag to indicate whether to use proxies
USE_PROXY_AUTH = True                    # Flag to indicate whether to use proxy authentication
USE_SELENIUM = False                     # Flag to indicate whether to use Selenium for scraping
USE_DEFAULT_CORE_NUMBER = False           # Flag to indicate whether to use the default core number

# MongoDB settings for data storage
USE_MONGODB = True                       # Flag to indicate whether to use MongoDB for data storage
USE_MONGODB_AUTH = False                 # Flag to indicate whether to use MongoDB authentication
MONGODB_SERVER = 'mongodb://127.0.0.1'   # MongoDB server address
MONGODB_PORT = 27017                     # MongoDB port number
MONGODB_DB = 'Log'                       # MongoDB database name
MONGODB_COLLECTION = 'facebook_page'     # MongoDB collection name
MONGODB_USERNAME = 'admin'               # MongoDB username
MONGODB_PASSWORD = '123456789'           # MongoDB password

# Logging and error handling settings
LOG_LEVEL = 3                            # Logging level
DISTRACTION_URL = 'https://www.google.com'        # URL to use for distraction during scraping
INITIALIZATION_URL = 'https://m.facebook.com'     # Initial URL for scraping
BASE_URL = 'https://m.facebook.com'                # Base URL for scraping
PAGE_LOAD_TIMEOUT = 60                   # Timeout for page loading
IMPLICITLY_WAIT = 60                     # Implicit wait time
REQUEST_TIMEOUT = 60                     # Timeout for web requests
SET_WINDOW_SIZE = True                   # Flag to indicate whether to set the window size for scraping
SET_WINDOW_POSITION = True               # Flag to indicate whether to set the window position for scraping
IGNORE_CERTIFICATE_ERRORS = True         # Flag to indicate whether to ignore SSL certificate errors
IGNORE_SSL_ERRORS = True                 # Flag to indicate whether to ignore SSL errors
ENABLE_AUTOMATION = True                 # Flag to indicate whether to enable browser automation
DISABLE_AUTOMATION_EXTENSION = True       # Flag to indicate whether to disable browser automation extension

# Log and file extensions
LOG_EXT = '.txt'                         # Log file extension
OUTPUT_EXT = '.csv'                      # Output file extension

# Chrome-specific settings
CHROME_LANGUAGE_CODE = 'en,en_US'        # Language code for Chrome browser
DEFAULT_WINDOWS_CHROME_DRIVER_PATH = 'libs/chromedriver.exe'    # Default Chrome driver path for Windows
DEFAULT_UBUNTU_CHROME_DRIVER_PATH = '/usr/bin/chromedriver'     # Default Chrome driver path for Ubuntu

# Facebook page scraping settings
GATEWAY_URL = 'https://www.facebook.com/pages/category/'         # URL for Facebook page category
ACCOUNT_BATCH_SIZE = 10                  # Batch size for processing Facebook accounts
REQUEST_BATCH_SIZE = 50                  # Batch size for web requests
NUMBER_THREAD = 4                        # Number of threads for processing
NUMBER_CORE = 12                         # Number of CPU cores
NUMBER_CATEGORIES_PER_VPS = 1524         # Number of categories per virtual private server

# MongoDB pipeline settings
REPEAT_INTERVAL = 1                      # Repeat interval for MongoDB pipeline operations
