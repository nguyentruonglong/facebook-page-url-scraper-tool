import hashlib
import platform
import random
import sys
import time
import zipfile
import requests
import settings
import urllib3

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from utils.io import DataIO

# Disable insecure request warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Downloader:
    """
    Handles downloading tasks.
    """

    def __init__(self, scraper):
        """
        Initializes the Downloader with a scraper instance.

        Args:
            scraper: The scraper instance.
        """
        self.scraper = scraper

    def create_proxy_extension(self, proxy_host, proxy_port, proxy_user, proxy_pass):
        """
        Processes proxy authentication settings and creates a Chrome extension.

        Args:
            proxy_host (str): Proxy host address.
            proxy_port (int): Proxy port.
            proxy_user (str): Proxy username.
            proxy_pass (str): Proxy password.

        Returns:
            str: Path to the created Chrome extension.
        """
        try:
            # Define the manifest JSON for the Chrome extension
            manifest_json = """
            {
                "version": "1.0.0",
                "manifest_version": 2,
                "name": "Chrome Proxy",
                "permissions": [
                    "proxy",
                    "tabs",
                    "unlimitedStorage",
                    "storage",
                    "<all_urls>",
                    "webRequest",
                    "webRequestBlocking"
                ],
                "background": {
                    "scripts": ["background.js"]
                },
                "minimum_chrome_version":"22.0.0"
            }
            """

            # Define the background JavaScript for the Chrome extension
            background_js = """
            var config = {
                    mode: "fixed_servers",
                    rules: {
                    singleProxy: {
                        scheme: "http",
                        host: "%s",
                        port: parseInt(%s)
                    },
                    bypassList: ["localhost"]
                    }
                };

            chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

            function callbackFn(details) {
                return {
                    authCredentials: {
                        username: "%s",
                        password: "%s"
                    }
                };
            }

            chrome.webRequest.onAuthRequired.addListener(
                        callbackFn,
                        {urls: ["<all_urls>"]},
                        ['blocking']
            );
            """ % (proxy_host, proxy_port, proxy_user, proxy_pass)

            # Generate a unique filename for the Chrome extension based on proxy settings
            postfix = f"{proxy_host}{proxy_port}{proxy_user}{proxy_pass}"
            postfix = hashlib.md5(bytes(postfix, 'utf-8')).hexdigest()
            plugin_filepath = f'plugins/proxy_auth_plugin_{postfix}.zip'

            # Create the Chrome extension ZIP file with manifest and background script
            with zipfile.ZipFile(plugin_filepath, 'w') as zip_file:
                zip_file.writestr("manifest.json", manifest_json)
                zip_file.writestr("background.js", background_js)

            return plugin_filepath  # Return the path to the created Chrome extension
        except Exception as e:
            current_class = self.__class__.__name__  # Get the class name
            # Create an error log with class name and error details
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
            return None  # Return None in case of an exception

    def initialize_chrome_driver(self, proxy_args=dict(), user_agent=str()):
        """
        Initializes a Chrome WebDriver with specified options.

        Args:
            proxy_args (dict): Proxy configuration.
            user_agent (str): User-Agent string.

        Returns:
            WebDriver: Initialized Chrome WebDriver.
        """
        chrome_driver = None
        try:
            chrome_options = Options()  # Create Chrome options object

            # Configure Chrome options based on settings
            if settings.IGNORE_CERTIFICATE_ERRORS:
                chrome_options.add_argument('--ignore-certificate-errors')

            if settings.IGNORE_SSL_ERRORS:
                chrome_options.add_argument('--ignore-ssl-errors')

            if settings.LOG_LEVEL:
                chrome_options.add_argument(
                    '--log-level=' + str(settings.LOG_LEVEL))

            if settings.USE_COOKIE:
                # Configure experimental flags for cookie handling
                experimental_flags = [
                    'same-site-by-default-cookies@1', 'cookies-without-same-site-must-be-secure@1']
                chromeLocal_state_prefs = {
                    'browser.enabled_labs_experiments': experimental_flags}
                chrome_options.add_experimental_option(
                    'localState', chromeLocal_state_prefs)

            if settings.ENABLE_AUTOMATION:
                # Exclude the 'enable-automation' switch
                chrome_options.add_experimental_option(
                    "excludeSwitches", ['enable-automation'])

            if settings.DISABLE_AUTOMATION_EXTENSION:
                # Disable the use of automation extensions
                chrome_options.add_experimental_option(
                    'useAutomationExtension', False)

            if settings.CHROME_LANGUAGE_CODE:
                # Configure language preferences for Chrome
                chrome_options.add_argument('--lang=en-US')
                chrome_options.add_argument('--lang=en-GB')
                chrome_options.add_experimental_option(
                    'prefs', {'intl.accept_languages': settings.CHROME_LANGUAGE_CODE})

            if settings.USE_PROXY:
                if settings.USE_PROXY_AUTH:
                    # Add a Chrome extension for proxy authentication
                    chrome_options.add_extension(
                        self.create_proxy_extension(**proxy_args))
                else:
                    # Set the proxy server without authentication
                    chrome_options.add_argument(
                        '--proxy-server=%s' % f'{proxy_args["proxy_host"]}:{proxy_args["proxy_port"]}')

            if settings.USE_USER_AGENT:
                # Set a custom User-Agent string
                chrome_options.add_argument('--user-agent=%s' % user_agent)

            # Initialize Chrome WebDriver based on the platform
            if platform.system() == 'Windows':
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_WINDOWS_CHROME_DRIVER_PATH, chrome_options=chrome_options)
            elif platform.system() == 'Linux':
                chrome_options.add_argument('--no-sandbox')
                chrome_options.add_argument('--disable-dev-shm-usage')
                chrome_options.add_argument('--start-maximized')
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_UBUNTU_CHROME_DRIVER_PATH, chrome_options=chrome_options)

            if settings.PAGE_LOAD_TIMEOUT:
                # Set a page load timeout
                chrome_driver.set_page_load_timeout(settings.PAGE_LOAD_TIMEOUT)

            if settings.IMPLICITLY_WAIT:
                # Set an implicit wait time
                chrome_driver.implicitly_wait(settings.IMPLICITLY_WAIT)

            if settings.SET_WINDOW_POSITION:
                # Set the window position to a random location
                chrome_driver.set_window_position(
                    random.randint(0, 400), random.randint(0, 500))

            if settings.SET_WINDOW_SIZE:
                # Set the window size to a random device width and height
                device_width = random.choice([640, 1242,
                                            480, 768, 640, 720, 1125, 540, 1280, 750])
                device_height = random.choice([640, 1242,
                                            480, 768, 640, 720, 1125, 540, 1280, 750])
                chrome_driver.set_window_size(device_width, device_height)

        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        finally:
            return chrome_driver  # Return the initialized Chrome WebDriver

    def close_chrome_driver(self, chrome_driver):
        """
        Closes the Chrome WebDriver if available.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.

        Returns:
            None
        """
        try:
            # Check if the 'close' method exists and is callable on the chrome_driver
            if hasattr(chrome_driver, 'close') and callable(chrome_driver.close):
                chrome_driver.close()  # Close the Chrome WebDriver
        except Exception as e:
            current_class = self.__class__.__name__  # Get the class name
            # Create an error log with class name and error details
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

        finally:
            return None  # Return None to indicate the function has completed

    def fetch_web_page(self, chrome_driver, url, sleep_before=0, sleep_after=0):
        """
        Fetches a web page using the Chrome WebDriver.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.
            url (str): URL of the web page to fetch.
            sleep_before (int): Sleep duration before fetching the page (in seconds).
            sleep_after (int): Sleep duration after fetching the page (in seconds).

        Returns:
            WebDriver: Chrome WebDriver after page fetch.
        """
        try:
            time.sleep(sleep_before)  # Pause execution for sleep_before seconds
            chrome_driver.get(url)  # Open the specified URL in the Chrome WebDriver
            time.sleep(sleep_after)  # Pause execution for sleep_after seconds after fetching the page
        except Exception as e:
            current_class = self.__class__.__name__  # Get the class name
            # Create an error log with class name and error details
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

        finally:
            return chrome_driver  # Return the Chrome WebDriver after fetching the page

    def initialize_request(self, cookies_list=list(), proxy_config=dict(), user_agent_str=str()):
        """
        Initializes an HTTP request session with custom settings.

        Args:
            cookies_list (list): List of cookies to include in the request.
            proxy_config (dict): Proxy configuration.
            user_agent_str (str): User-Agent string.

        Returns:
            requests.Session: Initialized HTTP request session.
        """
        initialized_session = None  # Initialize the session_request variable
        try:
            headers = dict()  # Create an empty dictionary for headers
            proxies = dict()  # Create an empty dictionary for proxies
            initialized_session = requests.Session()  # Create a new HTTP request session

            if settings.USE_COOKIE:
                # Check if cookie usage is enabled
                # Extract and set specific cookies in the session
                c_user_cookie = next(
                    (cookie for cookie in cookies_list if cookie.get('name', str()) == 'c_user'), dict())
                xs_cookie = next(
                    (cookie for cookie in cookies_list if cookie.get('name', str()) == 'xs'), dict())
                cookies = dict({
                    'c_user': c_user_cookie.get('value', str()),
                    'xs': xs_cookie.get('value', str()),
                })
                initialized_session.cookies.update(cookies)  # Update session cookies

            if settings.USE_PROXY:
                # Check if proxy usage is enabled
                # Set up proxy configuration for the request
                proxy_host = proxy_config['proxy_host']
                proxy_port = proxy_config['proxy_port']
                proxy_user = proxy_config['proxy_user']
                proxy_pass = proxy_config['proxy_pass']
                proxies = {
                    'https': f'https://{proxy_user}:{proxy_pass}@{proxy_host}:{proxy_port}',
                    'http': f'http://{proxy_user}:{proxy_pass}@{proxy_host}:{proxy_port}',
                }
                initialized_session.proxies.update(proxies)  # Update session proxies

            if settings.USE_USER_AGENT:
                # Check if custom User-Agent usage is enabled
                # Set the User-Agent header for the request
                headers.update({
                    'Content-Type': 'text/html; charset=utf-8',
                    'User-Agent': user_agent_str,
                })
                initialized_session.headers.update(headers)  # Update session headers

        except Exception as e:
            initialized_session = None  # Set session_request to None in case of an error
            current_class = self.__class__.__name__  # Get the class name
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

        finally:
            return initialized_session  # Return the initialized HTTP request session
