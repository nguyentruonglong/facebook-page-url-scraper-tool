import random
import sys
import time
import settings

from urllib.parse import urljoin
from threading import Thread
from bs4 import BeautifulSoup
from furl.furl import furl
from utils.io import DataIO


class DownloaderMiddleware:
    """
    Middleware for downloading content.
    """

    def __init__(self, scraper):
        """
        Constructor for the middleware.

        Args:
            scraper: The scraper object.
        """
        self.scraper = scraper  # Store the scraper object

    def extract_page_source(self, chrome_driver):
        """
        Extract the page source from the Chrome WebDriver.

        Args:
            chrome_driver: Chrome WebDriver instance.

        Returns:
            str: Page source as a string.
        """
        try:
            # Try to extract the page source from the Chrome WebDriver.
            page_source = chrome_driver.page_source
        except Exception as e:
            # If an exception occurs (e.g., WebDriver error), set page_source to an empty string.
            page_source = str()
            # Get the name of the current class (DownloaderMiddleware).
            current_class = self.__class__.__name__
            # Create a log message with information about the error.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Write the error message to a log file specified in the scraper's settings.
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        finally:
            # Return the page source (either extracted or an empty string).
            return page_source

    def add_facebook_cookies(self, chrome_driver, facebook_cookies):
        """
        Add Facebook cookies to the Chrome WebDriver.

        Args:
            chrome_driver: Chrome WebDriver instance.
            facebook_cookies: List of Facebook cookies.

        Returns:
            WebDriver: Chrome WebDriver after adding cookies.
        """
        try:
            # Load an initialization URL in the Chrome WebDriver and wait for 20 seconds (you might need to change this delay).
            chrome_driver.get(settings.INITIALIZATION_URL)
            time.sleep(20)
            # Iterate through the provided list of Facebook cookies.
            for cookie in facebook_cookies:
                # Add each cookie to the Chrome WebDriver's current session.
                chrome_driver.add_cookie(cookie)
        except Exception as e:
            # If an exception occurs (e.g., WebDriver error), handle it.
            current_class = self.__class__.__name__
            # Create a log message with information about the error.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Write the error message to a log file specified in the scraper's settings.
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        finally:
            # Return the Chrome WebDriver instance after adding cookies (or the original instance if an exception occurred).
            return chrome_driver


class ScraperMiddleware:
    """
    Middleware for web scraping.
    """

    def __init__(self, scraper):
        # Constructor for the middleware, takes a 'scraper' object as an argument.
        self.scraper = scraper

    def analyze_h2_level(self, h2_index, h2_wrapper, h2_wrappers):
        """
        Analyze H2 level content.

        Args:
            h2_index (int): Index of the H2 level content.
            h2_wrapper: Wrapper for the H2 level content.
            h2_wrappers: List of H2 level content wrappers.

        Returns:
            None
        """
        try:
            # Extract all H2 tags within the H2 level wrapper.
            h2_tags = list(h2_wrapper.find_elements_by_css_selector('h2'))
            if len(h2_tags) == 0:
                return

            # Get the text of the first H2 tag as the H2 level category.
            h2_category = h2_tags[0].text

            # Get the next element (H2 level body) in the list.
            h2_body = h2_wrappers[h2_index + 1]

            # Find all H3 level wrappers within the H2 level body.
            h3_wrappers = list(h2_body.find_elements_by_css_selector(':scope>div>*'))

            for h3_index, h3_wrapper in enumerate(h3_wrappers):
                # Extract all H3 tags within the H3 level wrapper.
                h3_tags = list(h3_wrapper.find_elements_by_css_selector('h3'))

                if len(h3_tags) == 0:
                    continue

                # Get the text of the first H3 tag as the H3 level category.
                h3_category = h3_tags[0].text

                # Get the next element (H3 level body) in the list.
                h3_body = h3_wrappers[h3_index + 1]
                sub_categories_dict = {}

                # Find all <a> elements within the H3 level body.
                sub_category_elements = list(h3_body.find_elements_by_tag_name('a'))

                for sub_category_element in sub_category_elements:
                    # Get the href attribute and text of each <a> element.
                    sub_category_url = sub_category_element.get_attribute('href')
                    sub_category_name = sub_category_element.text

                    # Store the sub-category information in a dictionary.
                    sub_categories_dict.update({
                        sub_category_url: sub_category_name
                    })

                for sub_category_url, sub_category_name in sub_categories_dict.items():
                    # Create a log entry with sub-category information and hierarchy.
                    log_content = f'{sub_category_url}, {sub_category_name}, {h3_category}, {h2_category}'
                    # Write the log entry to a log file specified in the scraper's settings.
                    DataIO().write_log_file(log_content, self.scraper.log_filepath)

        except Exception as e:
            # Handle exceptions and log errors.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_log_file(error_message, self.scraper.log_filepath)

    def extract_page_categories(self, chrome_driver):
        """
        Extract page categories from the Chrome WebDriver.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.

        Returns:
            None
        """
        try:
            thread_list = list()

            # Load the initial gateway URL in the Chrome WebDriver.
            chrome_driver.get(settings.GATEWAY_URL)

            # Find the element with id "content" in the loaded page.
            fb_content = chrome_driver.find_element_by_css_selector('#content')

            # Find all elements within the "content" element (div>div>*) and store them in h2_level_wrappers list.
            h2_level_wrappers = list(fb_content.find_elements_by_css_selector('#content>div>div>*'))

            for x, h2_level_wrapper in enumerate(h2_level_wrappers):
                # Create and start a thread to analyze H2 level content.
                thread = Thread(target=self.analyze_h2_level, args=[x, h2_level_wrapper, h2_level_wrappers])
                thread.setDaemon(True)  # Set the thread as a daemon thread.
                thread.start()
                thread_list.append(thread)

            for _ in range(len(thread_list)):
                # Wait for all threads to finish.
                thread = thread_list[_]
                thread.join()

        except Exception as e:
            # Handle exceptions and log errors.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

    def extract_facebook_page_items(self, chrome_driver, category_url, paging_url):
        """
        Extract Facebook page items from the Chrome WebDriver.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.
            category_url (str): URL of the category.
            paging_url (str): URL for paging.

        Returns:
            tuple: List of page items and ending status (True/False).
        """
        is_ending = False
        try:
            extracted_pages = list()  # List to store extracted page information
            page_url = str()
            page_name = str()

            # Combine CSS selectors to find page sections
            css_selectors = ['._6-4e', '#page_category_main_column']

            for selector in css_selectors:
                page_category_columns = list(chrome_driver.find_elements_by_css_selector(selector))

                if len(page_category_columns) > 0:
                    # Find page sections within the category columns.
                    page_sections = list(page_category_columns[0].find_elements_by_css_selector('._6-43'))
                    
                    for page_section in page_sections:
                        # Find the first "a" element within a "._6-3a" element (page title).
                        page_title = page_section.find_element_by_css_selector('._6-3a > a')
                        original_page_url = page_title.get_attribute('href')
                        page_url_structure = furl(original_page_url).asdict()

                        # Construct the Facebook page URL and extract page name.
                        page_url = f'{page_url_structure["origin"]}/{page_url_structure["path"]["segments"][0]}'
                        page_name = page_title.text
                        current_time = DataIO().get_current_time().strftime('%Y-%m-%d %H-%M-%S.%f')
                        print(f'{page_url} : {page_name}')

                        # Append the page information to the result list.
                        extracted_pages.append([page_url, page_name, category_url, paging_url, current_time, current_time])

                        if len(page_sections) == 0:
                            is_ending = True

        except Exception as e:
            # Handle exceptions, log errors, and return an empty result.
            extracted_pages = list()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        
        return extracted_pages, is_ending

    def check_is_blocked(self, chrome_driver, paging_url):
        """
        Check if a blocking status is encountered.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.
            paging_url (str): URL for paging.

        Returns:
            bool: True if blocking status is detected, False otherwise.
        """
        try:
            # Load the provided URL in the Chrome WebDriver.
            chrome_driver.get(paging_url)
            # Get the HTML page source of the loaded page.
            page_source = chrome_driver.page_source
            # Get the current URL of the WebDriver after loading the page.
            current_url = chrome_driver.current_url

            # Keywords indicating potential blocking in page source.
            page_source_keywords = ['Temporarily Blocked', 'ERR_TIMED_OUT', 'ERR_EMPTY_RESPONSE',
                                'ERR_CONNECTION_CLOSED', 'You must log in first', 'You must log in to continue',
                                'This page isn\'t available']

            # Check for various warning messages in the page source.
            for keyword in page_source_keywords:
                if keyword in page_source:
                    # If a warning is found in the page source, log it and return True (blocking status detected).
                    DataIO().write_to_log(keyword, self.scraper.log_filepath)
                    return True

            # Keywords indicating potential blocking in current URL.
            current_url_keywords = ['checkpoint', 'login', 'actor_experience', 'confirmemail']

            # Check for various warning messages in the current URL.
            for keyword in current_url_keywords:
                if keyword in current_url:
                    # If a warning is found in the current URL, log it and return True (blocking status detected).
                    DataIO().write_to_log(keyword, self.scraper.log_filepath)
                    return True

            # Specific warning message keywords in the page source.
            specific_page_source_keywords = ['help/contact/571927962827151', 'error/']

            # Check for specific warning messages in the page source.
            for keyword in specific_page_source_keywords:
                if keyword in page_source:
                    # If a warning is found in the page source, log it and return True (blocking status detected).
                    DataIO().write_to_log(keyword, self.scraper.log_filepath)
                    return True

            # If no blocking status is detected, return False.
            return False
        except Exception as e:
            # Handle exceptions, log errors, and assume blocking status by returning True.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
            return True

    def scrape_pages_in_category(self, chrome_driver, mongo_pipeline, category_url, start_page=1, missing_pages=list()):
        """
        Scrape pages within a category.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance.
            mongo_pipeline: MongoDB pipeline for processing scraped data.
            category_url (str): URL of the category.
            start_page (int): Starting page number.
            missing_pages (list): List of missing pages.

        Returns:
            tuple: List of page items, current page number, blocking status, ending status.
        """
        is_blocked = False
        is_ending = False
        current_page = start_page if len(missing_pages) == 0 else missing_pages.pop(0)

        try:
            while True:
                # Construct the paging URL
                paging_url = f'{category_url}?page={str(current_page)}'
                print(paging_url)

                # Check if the request is blocked
                is_blocked = self.check_is_blocked(chrome_driver, paging_url)

                if is_blocked:
                    # If the request is blocked
                    DataIO().capture_screenshot(chrome_driver, settings.OUTPUT_SCREENSHOT_DIR)
                    return [], current_page, is_blocked, is_ending

                # If not blocked, scrape Facebook page items at the current request
                page_items, is_ending = self.extract_facebook_page_items(chrome_driver, category_url, paging_url)

                if len(page_items) == 0:
                    # If no Facebook Page items are collected
                    DataIO().capture_screenshot(chrome_driver, settings.OUTPUT_SCREENSHOT_DIR)
                    if not is_ending:
                        is_blocked = True
                    break

                if mongo_pipeline is not None:
                    mongo_pipeline.process_facebook_page_items(page_items)

                chrome_driver.get(settings.DISTRACTION_URL)
                time.sleep(random.randint(6, 8))

                # Update the current page
                if len(missing_pages) == 0:
                    current_page = start_page if current_page < start_page else current_page + 1
                elif len(missing_pages) > 0:
                    current_page = missing_pages.pop(0)

                # Stop the loop after a certain number of consecutive requests to change accounts
                if current_page % settings.REQUEST_BATCH_SIZE == 0:
                    break

            return page_items, current_page, is_blocked, is_ending

        except Exception as e:
            # Handle exceptions, assume blocking status, and set ending status to False.
            is_blocked = True
            is_ending = False
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
            return [], current_page, is_blocked, is_ending


class HttpScraperMiddleware(ScraperMiddleware):
    """
    Middleware for HTTP-based web scraping.
    """

    def __init__(self, scraper):
        # Constructor for HttpScraperMiddleware, inheriting from ScraperMiddleware.
        super().__init__(scraper)

    def extract_page_items(self, page_source, category_url, paging_url):
        """
        Extract Facebook page items from HTML page source.

        Args:
            page_source (str): HTML page source.
            category_url (str): URL of the category.
            paging_url (str): URL for paging.

        Returns:
            tuple: List of page items and ending status (True/False).
        """
        is_ending = False
        page_items = list()

        try:
            fb_page_url = str()
            fb_page_name = str()

            # Parse the HTML page source using BeautifulSoup.
            soup = BeautifulSoup(page_source, 'html.parser')

            def find_page_sections(selector):
                sections = soup.select(selector)
                return list(sections[0].select('._6-43')) if sections else []

            # Find elements with class "_6-4e" (page category columns).
            page_sections = find_page_sections('._6-4e')

            for page_section in page_sections:
                # Select the first "a" element within a "._6-3a" element (page title).
                page_title = page_section.select_one('._6-3a > a[href]')
                original_page_url = urljoin(settings.BASE_URL, page_title['href'])
                page_url_structure = furl(original_page_url).asdict()

                # Construct the Facebook page URL and extract page name.
                fb_page_url = f'{page_url_structure["origin"]}/{page_url_structure["path"]["segments"][0]}'
                fb_page_name = page_title.get_text()
                current_time = DataIO().get_current_time().strftime('%Y-%m-%d %H-%M-%S.%f')
                print(f'{fb_page_url} : {fb_page_name}')

                # Append the page information to the result list.
                page_items.append([fb_page_url, fb_page_name, category_url, paging_url, current_time, current_time])

            if not page_sections:
                # If there are no elements with class "_6-4e," try finding elements with id "page_category_main_column."
                page_sections = find_page_sections('#page_category_main_column')

                for page_section in page_sections:
                    original_page_url = urljoin(settings.BASE_URL, page_section['href'])
                    page_url_structure = furl(original_page_url).asdict()

                    # Construct the Facebook page URL and extract page name.
                    fb_page_url = f'{page_url_structure["origin"]}/{page_url_structure["path"]["segments"][0]}'
                    fb_page_name = page_section.get_text()
                    current_time = DataIO().get_current_time().strftime('%Y-%m-%d %H-%M-%S.%f')
                    print(f'{fb_page_url} : {fb_page_name}')

                    # Append the page information to the result list.
                    page_items.append([fb_page_url, fb_page_name, category_url, paging_url, current_time, current_time])

            if not page_sections:
                is_ending = True

        except Exception as e:
            # Handle exceptions, log errors, and return an empty result.
            page_items = list()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        finally:
            return page_items, is_ending

    def check_blocking_status(self, http_request, paging_url):
        """
        Check if a blocking status is encountered via HTTP requests.

        Args:
            http_request: HTTP request object.
            paging_url (str): URL for paging.

        Returns:
            tuple: Blocking status (True/False) and page source.
        """
        page_source = str()
        is_blocked = False

        try:
            # Send an HTTP GET request to the paging URL.
            http_response = http_request.get(paging_url, timeout=settings.TIMEOUT)
            
            # Read the content of the HTTP response.
            page_source = http_response.content.decode('utf-8')
            current_url = http_response.url
            
            # Check for various warning messages in the page source.
            for warning in ['Temporarily Blocked', 'ERR_TIMED_OUT', 'ERR_EMPTY_RESPONSE', 
                            'ERR_CONNECTION_CLOSED', 'You must log in first', 
                            'You must log in to continue', "This page isn't available"]:
                if warning in page_source:
                    # Log a warning if a specific message is found in the page source.
                    DataIO().write_to_log(warning, self.scraper.log_filepath)
                    is_blocked = True
                    break

            # Check for various warning messages in the current URL.
            for warning in ['checkpoint', 'login', 'actor_experience', 'confirmemail']:
                if warning in current_url:
                    # Log a warning if a specific URL is detected.
                    DataIO().write_to_log(warning, self.scraper.log_filepath)
                    is_blocked = True
                    break

            # Check for specific warning messages in the page source.
            for warning in ['help/contact/571927962827151', 'error/']:
                if warning in page_source:
                    # Log a warning if a specific message is found in the page source.
                    DataIO().write_to_log(warning, self.scraper.log_filepath)
                    is_blocked = True
                    break

        except Exception as e:
            # Handle exceptions, log errors, and assume blocking status.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
            is_blocked = True

        return is_blocked, page_source

    def scrape_pages_in_category(self, http_request, mongo_pipeline, category_url, start_page=1, missing_pages=list()):
        """
        Scrape pages within a category using HTTP requests.

        Args:
            http_request: HTTP request object.
            mongo_pipeline: MongoDB pipeline for processing scraped data.
            category_url (str): URL of the category.
            start_page (int): Starting page number.
            missing_pages (list): List of missing pages.

        Returns:
            tuple: List of page items, current page number, blocking status, ending status, and page source.
        """
        current_page_number = start_page if len(missing_pages) == 0 else missing_pages.pop(0)

        try:
            while True:
                paging_url = f'{category_url}?page={str(current_page_number)}'
                print(paging_url)

                # Check if the request is blocked
                is_blocked, page_source = self.check_blocking_status(http_request, paging_url)
                
                if is_blocked:
                    return [], current_page_number, True, False, page_source

                # If not blocked, scrape Facebook page items at the current request
                page_items, is_ending = self.extract_facebook_page_items(page_source, category_url, paging_url)
                
                if len(page_items) == 0:
                    # Log the HTTP response content to investigate the reason
                    DataIO().log_web_page(page_source, settings.OUTPUT_RESPONSE_DIR)

                    # If it's not due to running out of data, it's an unusual block
                    if not is_ending:
                        return [], current_page_number, True, False, page_source

                if mongo_pipeline is not None:
                    mongo_pipeline.process_facebook_page_items(page_items)

                time.sleep(random.randint(6, 10))

                # If there are no more missing pages in the category
                if len(missing_pages) == 0:
                    current_page_number = max(start_page, current_page_number + 1)
                else:
                    current_page_number = missing_pages.pop(0)

                # Stop the loop after a certain number of consecutive requests to change accounts
                if current_page_number % settings.REQUEST_BATCH_SIZE == 0:
                    break
                    
            return page_items, current_page_number, False, is_ending, page_source

        except Exception as e:
            # Handle exceptions, assume blocking status, and set ending status to False.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
            return [], current_page_number, True, False, str()
