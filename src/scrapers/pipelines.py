import queue
import sys
import settings
import validators

from threading import Thread
from pymongo import MongoClient
from utils.io import DataIO


class MongoInsertThread(Thread):
    """
    A thread for inserting data into MongoDB.
    """

    def __init__(self, db_name, collection_name, data_items):
        """
        Initialize the MongoInsertThread.

        Args:
            db_name (str): MongoDB database name.
            collection_name (str): MongoDB collection name.
            data_items (list): List of items to insert into MongoDB.
        """
        # Initialize thread attributes with provided arguments
        self.db_name = db_name  # MongoDB database name
        self.collection_name = collection_name  # MongoDB collection name
        self.data_items = data_items  # List of items to insert into MongoDB
        Thread.__init__(self)  # Initialize the thread

    def run(self):
        try:
            item_objects = list()  # Initialize an empty list to store item objects

            # Define the field names for input items
            input_field_names = [
                'facebook_page_url', 'facebook_page_name',
                'category_url', 'paging_url', 'created_at', 'updated_at'
            ]

            for item in self.data_items:
                # Create a dictionary for each item using field names and item values
                item_object = dict(zip(input_field_names, item))
                facebook_page_url = item_object.get('facebook_page_url', str())  # Get the Facebook page URL
                if validators.url(facebook_page_url):  # Check if the URL is valid
                    item_objects.append(item_object)  # Append the item object to the list

            # Continue inserting the remaining items if any duplicate item exists
            self.collection_name.insert_many(item_objects, ordered=False)
        except Exception as e:
            class_name = self.__class__.__name__
            # Log and print any exceptions that occur during the insertion process
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            print(error_message)  # Print the error message


class MongoQuerysetThread(Thread):
    """
    A thread for querying data from MongoDB.
    """

    def __init__(self, db_name, collection_name, query_field, result_queue):
        """
        Initialize the MongoQuerysetThread.

        Args:
            db_name (str): MongoDB database name.
            collection_name (str): MongoDB collection name.
            query_field (str): Name of the field to query.
            result_queue (queue.Queue): Queue to store query results.
        """
        # Initialize thread attributes with provided arguments
        self.db_name = db_name  # MongoDB database name
        self.collection_name = collection_name  # MongoDB collection name
        self.query_field = query_field  # Name of the field to query
        self.result_queue = result_queue  # Queue to store query results
        Thread.__init__(self)  # Initialize the thread

    def run(self):
        try:
            # Execute a MongoDB aggregation to retrieve data based on the query_field
            query_cursor = self.collection_name.aggregate(
                pipeline=[
                    {"$group": {'_id': f"${self.query_field}"}},
                ],
                allowDiskUse=True  # Allow using disk for large aggregations
            )
            
            # Transform the results by replacing URLs and store them in a set
            queryset = set(
                map(lambda e: e.get('_id', str()).replace(
                    'https://www.facebook.com', 'https://m.facebook.com'), query_cursor))
            
            # Put the query results into the result queue for further processing
            self.result_queue.put(queryset)
        except Exception as e:
            class_name = self.__class__.__name__
            # Log and print any exceptions that occur during the query
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            print(error_message)  # Print the error message


class SpeedAnalysisThread(Thread):
    """
    A thread for analyzing the speed of data retrieval.
    """

    def __init__(self, db_name, collection_name, start_date, end_date):
        """
        Initialize the SpeedAnalysisThread.

        Args:
            db_name (str): MongoDB database name.
            collection_name (str): MongoDB collection name.
            start_date (datetime): Start date for analysis.
            end_date (datetime): End date for analysis.
        """
        # Initialize thread attributes with provided arguments
        self.db_name = db_name  # MongoDB database name
        self.collection_name = collection_name  # MongoDB collection name
        self.start_date = start_date  # Start date for analysis
        self.end_date = end_date  # End date for analysis
        Thread.__init__(self)  # Initialize the thread

    def run(self):
        try:
            # Perform MongoDB aggregation to analyze data retrieval speed
            count_query_cursor = self.collection_name.aggregate(
                pipeline=[
                    {
                        "$addFields": {
                            "substr_created_at": {"$substr": ["$created_at", 0, 23]},
                        }
                    },
                    {
                        "$addFields": {
                            "datetime_created_at": {
                                "$dateFromString": {
                                    "dateString": "$substr_created_at",
                                    "format": "%Y-%m-%d %H-%M-%S.%L"
                                }
                            }
                        }
                    },
                    {
                        "$match": {
                            "datetime_created_at": {
                                "$gte": self.start_date,
                                "$lte": self.end_date
                            }
                        }
                    },
                    {
                        "$count": "datetime_created_at"
                    }
                ],
                allowDiskUse=True  # Allow using disk for large aggregations
            )

            count_query_list = list(count_query_cursor)  # Convert cursor to a list
            total_facebook_pages = int(count_query_list[0].get(
                'datetime_created_at', 0)) if len(count_query_list) > 0 else 0

            # Generate a log message indicating the total Facebook Pages retrieved
            error_message = f'{self.end_date}: {total_facebook_pages} Facebook Pages'
            delta_time = self.end_date - self.start_date

            # Based on the time duration, determine the log file to write to
            if delta_time.total_seconds() == 60:
                DataIO().write_to_log(error_message, settings.MINUTELY_REPORT)
            if delta_time.total_seconds() == 3600:
                DataIO().write_to_log(error_message, settings.HOURLY_REPORT)
            if delta_time.total_seconds() == 86400:
                DataIO().write_to_log(error_message, settings.DAILY_REPORT)
        except Exception as e:
            class_name = self.__class__.__name__
            # Log and print any exceptions that occur during analysis
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            print(error_message)  # Print the error message


class MongoDBPipeline(object):
    """
    A MongoDB pipeline for handling data processing.
    """

    def __new__(cls, *args, **kwargs):
        # Check if MongoDB should be used based on project settings
        if settings.USE_MONGODB:
            return super(MongoDBPipeline, cls).__new__(cls)  # Create a new instance of MongoDBPipeline
        else:
            return None  # Return None if MongoDB is not enabled in settings

    def __init__(self, scraper):
        """
        Initialize the MongoDBPipeline.

        Args:
            scraper: The scraper instance.
        """
        if settings.USE_MONGODB_AUTH:
            # Establish a MongoDB connection with authentication
            connection = MongoClient(
                username=settings.MONGODB_USERNAME,
                password=settings.MONGODB_PASSWORD,
                host=settings.MONGODB_SERVER,
                port=settings.MONGODB_PORT,
                maxPoolSize=1000,
                waitQueueTimeoutMS=1000,
                waitQueueMultiple=1000
            )
        else:
            # Establish a MongoDB connection without authentication
            connection = MongoClient(
                host=settings.MONGODB_SERVER,
                port=settings.MONGODB_PORT,
                maxPoolSize=1000,
                waitQueueTimeoutMS=1000,
                waitQueueMultiple=1000
            )

        # Access the MongoDB database and collection specified in settings
        self.mongo_database = connection[settings.MONGODB_DB]
        self.mongo_collection = self.mongo_database[settings.MONGODB_COLLECTION]
        self.scraper = scraper  # Store the scraper instance for reference

    def process_facebook_page_items(self, items):
        """
        Process and insert Facebook page items into MongoDB.

        Args:
            items (list): List of items to insert into MongoDB.
        """
        try:
            # Create a thread for inserting data into MongoDB
            insert_thread = MongoInsertThread(
                self.mongo_database, self.mongo_collection, items)
            insert_thread.setDaemon(True)  # Set the thread as a daemon thread
            insert_thread.start()  # Start the thread
            insert_thread.join()  # Wait for the thread to finish
        except Exception as e:
            class_name = self.__class__.__name__
            # Log and handle exceptions related to data insertion
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

    def calculate_and_log_scraping_speed(self, start_date, end_date):
        """
        Process and analyze data retrieval speed.

        Args:
            start_date (datetime): Start date for analysis.
            end_date (datetime): End date for analysis.
        """
        try:
            # Create a thread for analyzing data retrieval speed
            speed_thread = SpeedAnalysisThread(
                self.mongo_database, self.mongo_collection, start_date, end_date)
            speed_thread.setDaemon(True)  # Set the thread as a daemon thread
            speed_thread.start()  # Start the thread
            speed_thread.join()  # Wait for the thread to finish
        except Exception as e:
            class_name = self.__class__.__name__
            # Log and handle exceptions related to speed analysis
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            DataIO().write_to_log(error_message, self.scraper.log_filepath)

    def process_dataset(self, query_field_name):
        """
        Process and retrieve a dataset from MongoDB.

        Args:
            query_field_name (str): Name of the field to query.

        Returns:
            list: List of unique query results.
        """
        try:
            result_queue = queue.Queue()  # Create a queue to store query results
            # Create a thread for querying data from MongoDB
            queryset_thread = MongoQuerysetThread(
                self.mongo_database, self.mongo_collection, query_field_name, result_queue)
            queryset_thread.setDaemon(True)  # Set the thread as a daemon thread
            queryset_thread.start()  # Start the thread
            queryset_thread.join()  # Wait for the thread to finish
            dataset = result_queue.get()  # Retrieve the query results from the queue
        except Exception as e:
            dataset = list()  # Initialize an empty list in case of an exception
            class_name = self.__class__.__name__
            # Log and handle exceptions related to dataset retrieval
            error_message = (
                f'({class_name} class) | An error has occurred at '
                f'line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            )
            DataIO().write_to_log(error_message, self.scraper.log_filepath)
        finally:
            return dataset  # Return the retrieved dataset or an empty list
