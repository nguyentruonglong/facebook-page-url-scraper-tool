import csv
import datetime
import json
import os
import random
import sys
import time
import settings
import pytz
import imgkit

from fake_useragent import UserAgent
from user_agents import parse


class DataIO:
    """
    Utility class for handling data input and output operations.
    """

    def get_current_time(self):
        """
        Get the current time with timezone information.

        Returns:
            datetime.datetime: Current time with timezone.
        """
        return datetime.datetime.now(pytz.timezone('UTC')).astimezone(
            pytz.timezone(settings.TIMEZONE)  # Get current time in the specified timezone
        )

    def write_to_log(self, log_content, log_filepath):
        """
        Write log content to a log file.

        Args:
            log_content (str): Log content to be written.
            log_filepath (str): Path to the log file.
        """
        try:
            log_file = open(log_filepath, 'a+', encoding='utf-8')  # Open log file in append mode
            current_time = self.get_current_time()
            formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')  # Format current time
            log_entry = f'{formatted_time} | {log_content}\n'  # Create log entry with timestamp
            print(log_entry)  # Print log entry to console
            log_file.write(log_entry)  # Write log entry to log file
            log_file.close()  # Close the log file
        except Exception as e:
            class_name = self.__class__.__name__
            error_message = f'({class_name}) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)  # Handle exceptions and print error messages

    def write_to_csv(self, rows, csv_filepath):
        """
        Write contents to a CSV file.

        Args:
            rows (list): List of rows to be written.
            csv_filepath (str): Path to the CSV file.
        """
        try:
            with open(csv_filepath, 'a+', newline='', encoding='utf-8') as csv_file:
                csv_writer = csv.writer(csv_file)  # Open CSV file for writing
                for row in rows:
                    csv_writer.writerow(row)  # Write each row to the CSV file
        except Exception as e:
            class_name = self.__class__.__name__
            error_message = f'({class_name}) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)  # Handle exceptions and print error messages

    def read_multi_dimensional_data(self, filepaths, num_columns=None, num_rows=None, newline_char='\n', col_separator=','):
        """
        Read data from multiple files and return a two-dimensional list.

        Args:
            filepaths (list): List of file paths to read data from.
            num_columns (int, optional): Number of columns to read. Defaults to None.
            num_rows (int, optional): Number of rows to read. Defaults to None.
            newline_char (str, optional): Newline character. Defaults to '\n'.
            col_separator (str, optional): Column separator character. Defaults to ','.

        Returns:
            list: Two-dimensional list of data.
        """
        try:
            two_dimensional_data = list()  # Initialize an empty two-dimensional list
            for filepath in filepaths:
                input_file = open(filepath, 'rt', encoding='utf-8')  # Open file for reading
                raw_data = input_file.read().strip()  # Read file contents and remove leading/trailing spaces
                lines = raw_data.split(newline_char)  # Split file contents into lines
                tmp_dimensional_data = list()  # Initialize temporary list for this file
                for line in lines:
                    cells = line.strip().split(col_separator)  # Split line into cells using separator
                    tmp_dimensional_data.append(cells)  # Append the row to the temporary list
                if num_rows is not None:
                    tmp_dimensional_data = tmp_dimensional_data[:num_rows]  # Limit the number of rows if specified
                if num_columns is not None:
                    tmp_dimensional_data = [entry[:num_columns] for entry in tmp_dimensional_data if len(entry) == num_columns]  # Filter rows by column count
                two_dimensional_data += tmp_dimensional_data  # Append the data from this file to the main two-dimensional list
                input_file.close()  # Close the file
        except Exception as e:
            two_dimensional_data = list()  # Handle exceptions and return an empty list
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            return two_dimensional_data  # Return the two-dimensional list containing the data

    def get_subdirectories(self, base_directory, extensions=list()):
        """
        Get a list of subdirectories within a directory.

        Args:
            base_directory (str): The base directory path.
            extensions (list, optional): List of file extensions to filter by. Defaults to [].

        Returns:
            list: List of subdirectory paths.
        """
        try:
            subdirectory_paths = list()  # Initialize an empty list for subdirectory paths
            absolute_base_directory = os.path.abspath(base_directory)  # Get the absolute path of the base directory
            directory_entries = os.scandir(absolute_base_directory)  # List entries in the directory

            if len(extensions) > 0:
                # Filter entries by file extension
                directory_entries = [entry for entry in directory_entries if os.path.splitext(entry.name)[1] in extensions]

            # Create a list of subdirectory paths
            subdirectory_paths = [os.path.join(base_directory, entry.name) for entry in directory_entries if entry.is_dir()]
        except Exception as e:
            subdirectory_paths = list()  # Handle exceptions and return an empty list
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            return subdirectory_paths  # Return the list of subdirectory paths

    def capture_screenshot(self, chrome_driver, output_directory):
        """
        Capture a screenshot using the Selenium Chrome driver.

        Args:
            chrome_driver: Selenium Chrome driver instance.
            output_directory (str): Directory where the screenshot will be saved.
        """
        try:
            current_time = self.get_current_time()
            timestamp = current_time.strftime('%Y-%m-%d_%H-%M-%S-%f')  # Format timestamp
            wait_time = 3  # Wait time in seconds
            
            # Wait for a specified time before capturing the screenshot
            time.sleep(wait_time)
            
            screenshot_path = os.path.abspath(f'{output_directory}/Screenshot_{timestamp}.png')  # Prepare screenshot path
            chrome_driver.save_screenshot(screenshot_path)  # Capture and save screenshot

            # Wait for a specified time after capturing the screenshot
            time.sleep(wait_time)
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)

    def log_web_page(self, html_content, output_directory):
        """
        Log a web page's HTML content and capture a screenshot.

        Args:
            html_content (str): HTML content of the web page.
            output_directory (str): Directory where logs and screenshots will be saved.
        """
        try:
            current_time = self.get_current_time()
            timestamp = current_time.strftime('%Y-%m-%d %H-%M-%S-%f')  # Format timestamp
            screenshot_path = os.path.abspath(f'{output_directory}/Screenshot_{timestamp}.png')  # Prepare screenshot path

            # Capture a screenshot from HTML content for debugging purposes
            imgkit.from_string(html_content, screenshot_path)  # Capture and save screenshot

            html_log_path = os.path.abspath(f'{output_directory}/Response_{timestamp}.html')  # Prepare HTML log path
            with open(html_log_path, 'a+', encoding='utf-8') as html_file:  # Open HTML log file for writing
                html_file.write(html_content)  # Write HTML content to log file
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)

    def load_page_categories(self, categories_filepath):
        """
        Load a list of page categories from a file.

        Args:
            categories_filepath (str): Path to the file containing page categories.

        Returns:
            list: List of page categories.
        """
        try:
            categories = list()  # Initialize an empty list for page categories
            categories_file = open(categories_filepath, 'rt', encoding='utf-8')  # Open categories file for reading
            raw_data = categories_file.read().strip()  # Read file contents and remove leading/trailing spaces
            categories = list(raw_data.split('\n'))  # Split file contents into lines
            categories = list(filter(lambda entry: entry != str() and entry != '\n', categories))  # Filter out empty lines
            categories_file.close()  # Close the file
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            categories = list()  # Handle exceptions and return an empty list
            if hasattr(categories_file, 'close') and callable(categories_file.close):
                categories_file.close()  # Close the file if it's open
        finally:
            return categories  # Return the list of page categories

    def load_extension_cookies(self, cookies_filepath):
        """
        Load a list of cookies from a file.

        Args:
            cookies_filepath (str): Path to the file containing cookies.

        Returns:
            list: List of cookies as dictionaries.
        """
        try:
            cookies_list = list()  # Initialize an empty list for cookies
            with open(cookies_filepath, 'rt', encoding='utf-8') as cookie_file:  # Open cookies file for reading
                file_contents = cookie_file.read()  # Read file contents
                cookie_lines = file_contents.split('\n')  # Split file contents into lines
                cookie_lines = list(filter(lambda entry: entry != str() and entry != '\n', cookie_lines))  # Filter out empty lines
                for cookie_content in cookie_lines:
                    cookie_data = {}  # Initialize an empty dictionary for cookie data
                    cookie_content = str(cookie_content).strip()  # Remove leading/trailing spaces
                    cookie_data = json.loads(cookie_content)  # Parse cookie data from JSON
                    cookies_list.append(cookie_data)  # Append the cookie data to the list
        except Exception as e:
            cookies_list = list()  # Handle exceptions and return an empty list
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            return cookies_list  # Return the list of cookies as dictionaries

    def preprocess_cookie(self, cookie_data):
        """
        Preprocess cookie data.

        Args:
            cookie_data (str): Cookie data to preprocess.

        Returns:
            str: Preprocessed cookie data.
        """
        try:
            fragments = cookie_data.split('||')  # Split cookie data using '||'
            value = fragments[0].split('|')[0]  # Extract the value portion
            preprocessed_cookie = f'value={value};{fragments[1]}'  # Format preprocessed cookie
            return preprocessed_cookie
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            return str()  # Handle exceptions and return an empty string

    def parse_cookie(self, input_data):
        """
        Parse cookie data and return a dictionary.

        Args:
            input_data (str): Cookie data to parse.

        Returns:
            dict: Parsed cookie data as a dictionary.
        """
        try:
            parsed_cookies = list()  # Initialize an empty list for parsed cookie data
            input_data = str(input_data).strip()  # Remove leading/trailing spaces
            cookie_items = input_data.split(';')  # Split cookie data into items
            for item in cookie_items:
                key, value = item.split('=')  # Split item into key and value
                if key in ['c_user', 'xs', 'fr', 'datr', 'sb']:
                    parsed_cookies.append({  # Add cookie data to the list
                        'domain': '.facebook.com',
                        'name': key,
                        'value': value
                    })
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            parsed_cookies = {}  # Handle exceptions and return an empty dictionary
        finally:
            return parsed_cookies  # Return the parsed cookie data as a

    def load_cookies(self, cookies_filepath):
        """
        Load a list of cookies from a file.

        Args:
            cookies_filepath (str): Path to the file containing cookies.

        Returns:
            list: List of cookies as dictionaries.
        """
        try:
            loaded_cookies = list()  # Initialize an empty list for cookies
            cookie_file = open(cookies_filepath, 'rt', encoding='utf-8')  # Open cookies file for reading
            file_contents = cookie_file.read()  # Read file contents
            file_contents = file_contents.replace('\r', '\n')  # Replace carriage returns with line feeds
            cookie_lines = file_contents.split('\n')  # Split file contents into lines
            non_empty_lines = list(filter(lambda entry: entry != str() and entry != '\n', cookie_lines))  # Filter out empty lines
            loaded_cookies = list(map(lambda entry: self.parse_cookie(entry), non_empty_lines))  # Parse each cookie and add to the list
            random.shuffle(loaded_cookies)  # Shuffle the list of cookies
            cookie_file.close()  # Close the file
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            loaded_cookies = list()  # Handle exceptions and return an empty list
        finally:
            return loaded_cookies  # Return the list of cookies as dictionaries

    def parse_proxy(self, proxy_info):
        """
        Parse proxy information from a string.

        Args:
            proxy_info (str): Proxy information as a string.

        Returns:
            dict: Parsed proxy information as a dictionary.
        """
        try:
            parsed_proxy_info = {}  # Initialize an empty dictionary for parsed proxy data
            proxy_info = proxy_info.strip()  # Remove leading/trailing spaces
            fragments = proxy_info.split(':')  # Split proxy data using ':'
            if len(fragments) == 4:
                parsed_proxy_info.update({  # Update parsed_proxy_info dictionary with parsed values
                    'proxy_host': fragments[0],
                    'proxy_port': fragments[1],
                    'proxy_user': fragments[2],
                    'proxy_pass': fragments[3],
                })
            elif len(fragments) == 2:
                parsed_proxy_info.update({  # Update parsed_proxy_info dictionary with parsed values
                    'proxy_host': fragments[0],
                    'proxy_port': fragments[1],
                })
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            parsed_proxy_info = {}  # Handle exceptions and return an empty dictionary
        finally:
            return parsed_proxy_info  # Return the parsed proxy data as a dictionary

    def load_proxies(self, proxies_filepath, repetition_ratio=1):
        """
        Load a list of proxy configurations from a file.

        Args:
            proxies_filepath (str): Path to the file containing proxy configurations.
            repetition_ratio (int, optional): The number of times each proxy should be repeated. Defaults to 1.

        Returns:
            list: List of proxy configurations as dictionaries.
        """
        try:
            proxy_configurations = list()  # Initialize an empty list for proxy configurations
            proxies_file = open(proxies_filepath, 'rt', encoding='utf-8')  # Open proxies file for reading
            raw_data = proxies_file.read().strip()  # Read file contents and remove leading/trailing spaces
            raw_data = raw_data.replace('\r', '\n')  # Replace carriage returns with line feeds
            proxy_rows = list(raw_data.split('\n'))  # Split file contents into lines
            proxy_rows = list(filter(lambda entry: entry != str() and entry != '\n', proxy_rows))  # Filter out empty lines
            tmp_proxies = list(map(lambda entry: self.parse_proxy(entry), proxy_rows))  # Parse each proxy and add to the list
            for proxy in tmp_proxies:
                proxy_configurations += list([proxy] * repetition_ratio)  # Repeat each proxy according to the specified ratio
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            proxy_configurations = list()  # Handle exceptions and return an empty list
        finally:
            return proxy_configurations  # Return the list of proxy configurations as dictionaries

    def load_user_agents(self, user_agents_filepath):
        """
        Load a list of user agents from a file.

        Args:
            user_agents_filepath (str): Path to the file containing user agents.

        Returns:
            list: List of user agents.
        """
        try:
            loaded_user_agents = list()  # Initialize an empty list for user agents
            user_agents_file = open(user_agents_filepath, 'rt', encoding='utf-8')  # Open user agents file for reading
            file_contents = user_agents_file.read()  # Read file contents
            loaded_user_agents = file_contents.split('\n')  # Split file contents into lines
            loaded_user_agents = list(filter(lambda entry: entry != str() and entry != '\n', loaded_user_agents))  # Filter out empty lines
            loaded_user_agents = list(map(lambda entry: str(entry).strip(), loaded_user_agents))  # Remove leading/trailing spaces from user agents
            pc_user_agents = list(filter(lambda entry: parse(entry).is_pc == True, loaded_user_agents))  # Filter out non-PC user agents
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            loaded_user_agents = list()  # Handle exceptions and return an empty list
        finally:
            return pc_user_agents  # Return the list of PC user agents

    def generate_random_user_agent(self):
        """
        Generate a random User-Agent string.

        Returns:
            str: Random User-Agent string.
        """
        try:
            random_user_agent = str()  # Initialize an empty string for the user agent
            ua_generator = UserAgent()  # Create a UserAgent generator instance
            random_user_agent = ua_generator.random  # Generate a random User-Agent string
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            random_user_agent = str()  # Handle exceptions and return an empty string
        finally:
            return random_user_agent  # Return the generated User-Agent string
